package magicgoose.tipanimationtestproject.api

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import magicgoose.tipanimationtestproject.api.data.RedditUser
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

class RedditUserApi {
    private interface RedditUserRetrofitApi {
        @GET("user/{name}/about.json")
        fun getUserInfo(@Path("name") username: String): Single<RedditUser>
    }

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://www.reddit.com/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val apiImpl = retrofit.create(RedditUserRetrofitApi::class.java)

    fun getUserInfo(username: String): Single<RedditUser> {
        return apiImpl.getUserInfo(username).observeOn(AndroidSchedulers.mainThread())
    }
}