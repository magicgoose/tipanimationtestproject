package magicgoose.tipanimationtestproject.api.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class RedditUser(
    @SerializedName("data")
    val data: RedditUserData
)


@Parcelize
data class RedditUserData(
    @SerializedName("name")
    val name: String,
    @SerializedName("icon_img")
    val iconImageUrl: String
): Parcelable