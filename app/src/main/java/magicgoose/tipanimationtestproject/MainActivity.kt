package magicgoose.tipanimationtestproject

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import magicgoose.tipanimationtestproject.api.RedditUserApi
import magicgoose.tipanimationtestproject.api.data.RedditUser
import magicgoose.tipanimationtestproject.api.data.RedditUserData
import java.math.BigDecimal
import java.util.*

const val userDataBundleKey = "userData"
const val totalMoneyBundleKey = "totalMoneyCollected"

class MainActivity : AppCompatActivity(), TipFragment.OnFragmentInteractionListener {
    val rng = Random()

    // normally I'd cache it in some way to not create it every time with the activity, but for the sake of simplicity...
    private val api by lazy { RedditUserApi() }

    private var userDataCall: Disposable? = null
    private var userData: RedditUserData? = null

    private var tipAmount: BigDecimal? = null
    private var totalMoneyCollected: BigDecimal = BigDecimal.ZERO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tip_button.setOnClickListener(this::onTipButtonClick)

        userData = savedInstanceState?.getParcelable(userDataBundleKey)
        totalMoneyCollected = savedInstanceState?.getSerializable(totalMoneyBundleKey) as? BigDecimal ?: BigDecimal.ZERO
        updateTotalMoneyView()
        if (userData == null) {
            this.userDataCall = api.getUserInfo("ketralnis").subscribe(this::onGetUserInfoOK, this::onGetUserInfoFail)
        }
    }

    override fun onDestroy() {
        userDataCall?.dispose()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (userData != null) {
            outState.putParcelable(userDataBundleKey, this.userData)
            outState.putSerializable(totalMoneyBundleKey, this.totalMoneyCollected)
        }
    }

    private fun onGetUserInfoOK(result: RedditUser) {
        this.userData = result.data
    }

    private fun onGetUserInfoFail(ex: Throwable) {
        Toast.makeText(this, "Reddit API call failed (${ex})", Toast.LENGTH_SHORT).show()
    }

    @Suppress("UNUSED_PARAMETER")
    private fun onTipButtonClick(v: View) {
        val userData = this.userData
        if (userData == null) {
            Toast.makeText(this, "Please wait for the API call to finish", Toast.LENGTH_SHORT).show()
            return
        }

        TipFragment.show(
            supportFragmentManager,
            userData
        )
    }

    override fun tip(amount: BigDecimal) {
        tipAmount = amount
        val delay = rng.nextInt(2000) + 2000
        Handler().postDelayed(this::onTipComplete, delay.toLong())
    }

    @SuppressLint("SetTextI18n")
    private fun onTipComplete() {
        supportFragmentManager.findFragmentByTag(TipFragment.tag)?.run {
            supportFragmentManager.beginTransaction().remove(this).commitAllowingStateLoss()
        }
        totalMoneyCollected += tipAmount!!
        updateTotalMoneyView()
        showTipCompleteAnimation(tipAmount!!)
    }

    private fun showTipCompleteAnimation(tipAmount: BigDecimal) {
        val tipHeight = resources.getDimensionPixelSize(R.dimen.tip_complete_popup_height)

        tip_complete_popup.run {
            text = tipAmount.toPlainString()
            visibility = View.VISIBLE
            alpha = 0.5f
            scaleX = 0.5f
            scaleY = 0.5f
            translationY = tipHeight.toFloat()

            animate()
                .alpha(1f)
                .scaleX(1f)
                .scaleY(1f)
                .translationY(0f)
                .setStartDelay(200)
                .setDuration(400)
                .withEndAction {
                    animate()
                        .alpha(0f)
                        .scaleX(2f)
                        .scaleY(2f)
                        .translationY(-tipHeight * 2f)
                        .setStartDelay(1000)
                        .setDuration(300)
                        .withEndAction {
                            visibility = View.GONE
                        }
                        .start()
                }
                .start()
        }
    }

    private fun updateTotalMoneyView() {
        if (totalMoneyCollected > BigDecimal.ZERO) {
            tip_counter.text = totalMoneyCollected.toPlainString()
            tip_counter.visibility = View.VISIBLE
        }
    }
}
