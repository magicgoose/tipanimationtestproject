package magicgoose.tipanimationtestproject

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_tip.view.*
import magicgoose.tipanimationtestproject.api.data.RedditUserData
import magicgoose.tipanimationtestproject.util.forEachCodePoint
import magicgoose.tipanimationtestproject.view.SpinningCoinView
import java.math.BigDecimal
import java.util.regex.Pattern
import kotlin.math.max
import kotlin.math.min

const val swipeDismissThreshold = 0.5f

class TipFragment : DialogFragment(), View.OnTouchListener, View.OnClickListener, TextWatcher {
    private var listener: OnFragmentInteractionListener? = null
    private var dialogLayout: ViewGroup? = null
    private var paymentMethodsLayout: ViewGroup? = null
    private var paymentMethodViews: List<TextView> = emptyList()
    private var paymentMethodsLine: View? = null
    private var tipButtonExtraSpace: Space? = null
    private var tipAmountEdittext: EditText? = null
    private var tipAnimationBackgroundView: View? = null
    private var tipAnimationCoinView: SpinningCoinView? = null

    private var selectedPaymentMethodIndex: Int = 0
    private var paymentMethodsExpanded: Boolean = false

    private lateinit var userData: RedditUserData

    companion object {
        const val tag = "TipFragment"

        private val minAmount = BigDecimal.ONE
        private val maxAmount = BigDecimal.valueOf(100)

        const val userDataArgBundleKey = "userData"

        private val acceptedTextInputPattern = Pattern.compile("\\\$(?:[0-9]|\\.)*")

        fun show(fm: FragmentManager, userData: RedditUserData) {
            TipFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(userDataArgBundleKey, userData)
                }
            }.show(fm, tag)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog.window!!.attributes.windowAnimations = R.style.TipDialogAnimation
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.DialogStyle)
        this.userData = arguments!!.getParcelable(userDataArgBundleKey)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog.setCanceledOnTouchOutside(true)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        return inflater.inflate(R.layout.fragment_tip, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dialogLayout = view.dialog_layout
        this.dialogLayout = dialogLayout
        dialogLayout.setOnTouchListener(this)

        view.clipToOutline = true
        view.button_close.setOnClickListener { dismiss() }
        view.avatar_imageview.run {
            clipToOutline = true
            loadAvatarInto(this)
        }
        view.plus_button.run {
            clipToOutline = true
            setOnClickListener(this@TipFragment)
        }
        view.minus_button.run {
            clipToOutline = true
            setOnClickListener(this@TipFragment)
        }
        view.tip_button.run {
            clipToOutline = true
            setOnClickListener(this@TipFragment)
            text = getString(R.string.tip_button_with_username, userData.name)
        }
        view.username_view.text = userData.name

        view.terms_notice.text = TextUtils.concat(
            getString(R.string.terms_notice_1),
            SpannableString(getString(R.string.terms_notice_2)).apply {
                setSpan(
                    ForegroundColorSpan(ContextCompat.getColor(view.context, R.color.url_span_color)),
                    0, length, 0
                )
            })

        this.tipAmountEdittext = view.tip_amount_edittext
        this.paymentMethodsLine = view.payment_method_line
        this.paymentMethodsLayout = view.payment_methods_layout
        this.paymentMethodViews = listOf(
            view.payment_method_1,
            view.payment_method_2,
            view.payment_method_add
        )
        this.tipButtonExtraSpace = view.tip_button_space

        this.paymentMethodViews.forEachIndexed { i, v ->
            if (v is Checkable) {
                v.setOnClickListener(this@TipFragment)
            }
        }
        this.tipAnimationBackgroundView = view.tip_animation_bg
        this.tipAnimationCoinView = view.spinning_coin

        setPaymentMethodSelected(0)

        view.tip_amount_edittext.addTextChangedListener(this)
    }

    override fun onDestroyView() {
        dialogLayout = null
        paymentMethodsLayout = null
        paymentMethodViews = emptyList()
        paymentMethodsLine = null
        tipButtonExtraSpace = null
        tipAmountEdittext = null
        tipAnimationBackgroundView = null
        tipAnimationCoinView = null
        super.onDestroyView()
    }

    private fun loadAvatarInto(imageView: ImageView) {
        Picasso.get().load(userData.iconImageUrl).into(imageView)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tip_button -> {
                onTipClick()
                return
            }
            R.id.plus_button -> {
                onModifyButtonClick(1)
                return
            }
            R.id.minus_button -> {
                onModifyButtonClick(-1)
                return
            }
        }

        if (paymentMethodsExpanded) {
            setPaymentMethodSelected(paymentMethodViews.indexOf(v))
        } else {
            expandPaymentMethods()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun onModifyButtonClick(change: Int) {
        var numericValue = getTipAmountNumeric(tipAmountEdittext!!.text) ?: return
        numericValue = numericValue.add(BigDecimal.valueOf(change.toLong()))
        val result = numericValue.max(minAmount).min(maxAmount)
        tipAmountEdittext!!.setText('$' + result.toPlainString())
    }

    private fun onTipClick() {
        val text = tipAmountEdittext!!.text
        val numericValue = getTipAmountNumeric(text)
        if (numericValue == null) {
            Toast.makeText(context, R.string.error_invalid_number, Toast.LENGTH_SHORT).show()
            return
        }
        if (numericValue < minAmount || numericValue > maxAmount) {
            Toast.makeText(context, R.string.error_number_out_of_range, Toast.LENGTH_SHORT).show()
            return
        }
        listener!!.tip(numericValue)
        startTippingAnimation(text)
    }

    private fun startTippingAnimation(tipAmountText: CharSequence) {
        val tipAnimationCoinView = tipAnimationCoinView!!
        val tipAnimationBackgroundView = tipAnimationBackgroundView!!

        tipAnimationCoinView.setText(tipAmountText)
        tipAnimationBackgroundView.visibility = View.VISIBLE
        tipAnimationCoinView.visibility = View.VISIBLE

        tipAnimationBackgroundView.alpha = 0f
        tipAnimationBackgroundView.animate().alpha(1f).setDuration(1000)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .start()
        tipAnimationCoinView.translationY = resources.getDimensionPixelSize(R.dimen.coin_size).toFloat()
        tipAnimationCoinView.scaleX = 0.5f
        tipAnimationCoinView.scaleY = 0.5f
        tipAnimationCoinView.alpha = 0f
        tipAnimationCoinView.animate()
            .alpha(1f)
            .scaleX(1f)
            .scaleY(1f)
            .translationY(0f)
            .setDuration(1000)
            .setInterpolator(DecelerateInterpolator())
            .start()
    }

    private fun getTipAmountNumeric(s: CharSequence): BigDecimal? {
        return try {
            BigDecimal(s.substring(1))
        } catch (ex: NumberFormatException) {
            null
        }
    }

    private fun expandPaymentMethods() {
        paymentMethodsExpanded = true
        paymentMethodsLayout!!.setBackgroundResource(R.drawable.payment_methods_bg)
        paymentMethodsLine!!.visibility = View.INVISIBLE
        paymentMethodViews.forEachIndexed { i, v ->
            v.visibility = View.VISIBLE
            if (v is Checkable) {
                v.isChecked = selectedPaymentMethodIndex == i
                v.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_lock_dark, 0, 0, 0)
            }
        }
        tipButtonExtraSpace!!.visibility = View.INVISIBLE
    }

    private fun setPaymentMethodSelected(index: Int) {
        selectedPaymentMethodIndex = index
        paymentMethodsExpanded = false
        paymentMethodViews.forEachIndexed { i, v ->
            v.visibility = if (index == i) View.VISIBLE else View.GONE
            if (v is Checkable) {
                v.isChecked = false
                v.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_lock_gray, 0, 0, 0)
            }
        }
        paymentMethodsLayout!!.setBackgroundResource(0)
        paymentMethodsLine!!.visibility = View.VISIBLE
        tipButtonExtraSpace!!.visibility = View.GONE
    }

    private var dialogYTouchStart = 0f

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View, event: MotionEvent): Boolean {
        check(v == this.dialogLayout)
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                dialogYTouchStart = event.rawY
            }
            MotionEvent.ACTION_UP -> {
                if (event.rawY - dialogYTouchStart > v.height * swipeDismissThreshold) {
                    dismiss()
                } else {
                    v.translationY = 0f
                }
            }
            MotionEvent.ACTION_MOVE -> {
                v.translationY = Math.max(0f, event.rawY - dialogYTouchStart)
            }
        }
        return true
    }

    private var rewritingInput = false

    override fun afterTextChanged(s: Editable) {
        if (!rewritingInput && !acceptedTextInputPattern.matcher(s).matches()) {
            rewritingInput = true
            val edittext = tipAmountEdittext!!
            val selectionStart = edittext.selectionStart
            val selectionEnd = edittext.selectionEnd
            val sb = StringBuilder("$")
            s.toString().forEachCodePoint {
                if (Character.isDigit(it) || it == '.'.toInt()) {
                    sb.appendCodePoint(it)
                }
            }
            s.clear()
            s.append(sb)
            edittext.setSelection(
                max(1, min(s.length, selectionStart)),
                max(1, min(s.length, selectionEnd))
            )
            rewritingInput = false
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun tip(amount: BigDecimal)
    }
}

