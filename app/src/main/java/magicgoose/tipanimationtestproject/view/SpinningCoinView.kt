package magicgoose.tipanimationtestproject.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.spinning_coin.view.*
import magicgoose.tipanimationtestproject.R
import kotlin.math.PI

private const val animationTimePeriod = 1000000000L * 2
private const val coinThicknessRelative = 0.05f
private const val coinThicknessRelative2x = coinThicknessRelative * 2


class SpinningCoinView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {


    private val frontView by lazy { this.coin_imageview_front }
    private val backView by lazy { this.coin_imageview_back }
    private val fillerView by lazy { this.coin_filler }
    private var time = System.nanoTime()
    private var animationTime = 0L
    private var isFrontFacing = true

    private val size by lazy { resources.getDimensionPixelSize(R.dimen.coin_size) }

    fun setText(text: CharSequence) {
        frontView.text = text
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.spinning_coin, this, true)
        switchSides(frontView, backView)
        clipChildren = false
        clipToPadding = false
    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        updateAnimation()
        invalidate()
    }

    private fun updateAnimation() {
        val nextTime = System.nanoTime()
        val timeDiff = nextTime - time
        if (timeDiff <= 0) {
            return
        }

        time = nextTime
        animationTime = (animationTime + timeDiff) % animationTimePeriod

        val animationTimeNormalized = animationTime.toDouble() / animationTimePeriod

        val animationTime2PI = 2 * PI * animationTimeNormalized

        val frontHorizontalScale = Math.cos(animationTime2PI).toFloat()
        val displacementNormalized = Math.sin(animationTime2PI).toFloat()
        val displacement = displacementNormalized * size * coinThicknessRelative

        val willBeFrontFacing = frontHorizontalScale > 0
        if (willBeFrontFacing != isFrontFacing) {
            isFrontFacing = willBeFrontFacing
            if (isFrontFacing) {
                switchSides(frontView, backView)
            } else {
                switchSides(backView, frontView)
            }
        }
        frontView.scaleX = frontHorizontalScale
        backView.scaleX = -frontHorizontalScale
        frontView.translationX = -displacement
        backView.translationX = displacement
        fillerView.scaleX = displacementNormalized * coinThicknessRelative2x
    }

    private fun switchSides(frontView: View, backView: View) {
        frontView.bringToFront()
        if (frontView is ImageView) {
            frontView.imageAlpha = 255
        } else if (frontView is TextView) {
            frontView.setTextColor(Color.WHITE)
        }
        frontView.backgroundTintList = ContextCompat.getColorStateList(context, R.color.coin_front)
        if (backView is ImageView) {
            backView.imageAlpha = 0
        } else if (backView is TextView) {
            backView.setTextColor(Color.TRANSPARENT)
        }
        backView.backgroundTintList = ContextCompat.getColorStateList(context, R.color.coin_back)
    }
}