package magicgoose.tipanimationtestproject.util

inline fun String.forEachCodePoint(f: (Int) -> Unit) {
    val length = this.length
    var offset = 0
    while (offset < length) {
        val cp = this.codePointAt(offset)
        f(cp)
        offset += Character.charCount(cp)
    }
}
